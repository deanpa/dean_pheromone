#!/usr/bin/env python

import os
from pheromone import params
from pheromone import calculation

inputDataPath = os.path.join(os.getenv('PHERO', default='.'), 
        'Data')
outputDataPath = os.path.join(os.getenv('PHERO', default='.'), 
        'ResultsSingle', 'trial1')

pars = params.PheromoneParams()
pars.setExtentMask(os.path.join(inputDataPath, 'ressy.img'))
print('path', os.path.join(inputDataPath, 'ressy.img'))
pars.setTrapsFile(os.path.join(inputDataPath, 'ressyalldatatraploc5.csv'))

calculation.runModel(pars, savePath=outputDataPath, save=True)
#calculation.runModel(pars, savePath=outputDataPath, save=False)



