\documentclass[11pt, a4paper]{article}
\usepackage{geometry}
\geometry{a4paper, portrait, margin=1.2in}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{natbib}
\usepackage{pdfpages}

\captionsetup[table]{labelsep=space, 
         justification=raggedright, singlelinecheck=false}
                  
\usepackage[export]{adjustbox}
\usepackage{booktabs}

\graphicspath{{/home/dean/workfolder/projects/dean_pheromone}}
%\graphicspath{{./images/}}


\title{\textbf{Exploiting Allee effects with pheromone pollution to eradicate invasive predators}}
\author{Dean Anderson$^*$, Sam Gillingham}
\begin{document}
\maketitle

\begin{flushleft}

\vspace{0.5cm}

\begin{large}
\textbf{Manaaki Whenua Landcare Research}\\
$^*$andersond@landcareresearch.co.nz

\vspace{0.5cm}

\textbf{Initiate the model}
\end{large}
\end{flushleft}

\noindent
Time step = 0.5 hour for 8 hours per day.\\
Time extent = 3 years\\
Minimum initial population = 1 male and 1 female\\

We use the parameter "\textit{meanNAdd}" (\textit{e.g. 3}) to determine the random number of males and females to add to the the initial population. We use the following two-step process:

\begin{equation}
nAdd \sim Poisson(meanNAdd)
\end{equation}
\begin{equation}
nFemales \sim binomial(nAdd, \, 0.5)
\end{equation}
\begin{equation}
nMales = nAdd - nFemales
\end{equation}


\textbf{•}\begin{flushleft}
\begin{large}
\textbf{Pheromone decoy deployment}
\end{large}
\end{flushleft}
Pheromone decoys are deployed with at grid points at one of the four following spacings: 250, 500, 750 or 1000 m. With each iteration, one of the 4 spacings is chosen at random. The decoys can be deployed either one or two times during the estrous period (15 September - 15 January). The number of times will be chosen at random with each iteration. Pheromone decoys will always be deployed on 15 September, and if two deployment times is randomly chosen, the second deployment will be on 15 November, or half way through the estrous period. The attractiveness of the decoys will decay with time, which is determined by the $\gamma$ parameter (set in params). The effect of this on stoat movement is described under the "Mate searching movement" section.



\textbf{•}\begin{flushleft}
\begin{large}
\textbf{Movement states}
\begin{enumerate}
\item \textbf{non-estrous period (16 January - 14 September)}: all stoats exhibit homerange movement behaviour.
\item \textbf{Males in estrous period (15 September - 15 January)} exhibit mate searching behaviour throughout this period.
\item \textbf{Females in estrous period} exhibit mate searching behaviour if not pregnant on 15 September and remain in this state until they either become pregnant or the end of estrous period on 15 January. If they do not start out pregnant and become pregnant, they resume homerange behaviour.
\end{enumerate}
\end{large}
\end{flushleft}




\textbf{•}\begin{flushleft}
\begin{large}
\textbf{Homerange movement model}
\end{large}
\end{flushleft}


For $stoat_i$ the movment steps are modelled as a time series of (sub-day steps; \textit{e.g.} 30 min) steps, which are determined by random draws of step lengths (Figure \ref{fig:stepLength}) and turning angles (Figure \ref{fig:turnangles}) from Weibull and von Mises distributions, respectively.

\begin{equation}
stepLength_{i,t} \sim Weibull(shape, scale)
\end{equation}

\noindent
where shape = 1.0  and scale = 70.0. In Python this can be coded as: 

\begin{equation}
stepLength_{i,t} = scale * np.random.weibull(shape, size = n)
\end{equation}

\begin{figure}[h!]
\includegraphics[scale=.50, left]{stepLengthWeibull.png}
\caption{Histogram of step lengths (m) of from Weibull distribution with shape = 1.0  and scale = 70.0.}
\label{fig:stepLength}
\end{figure}



\begin{figure}[h!]
\includegraphics[scale=.50, left]{turningDensity.png}
\caption{Probability density of turning angles when the COA has the bearing of $\pi / 2$ and distance of 700 m. (A) The probability density for homerange movement behaviour with $\alpha = 0.075$ (eq. \ref{eq:hrKappa}). (B) The density for mate searching movement (BRW) with $\delta = 0.005$ and 0 days of deployment (eq. \ref{eq:matesearchTA}).}
\label{fig:turnangles}
\end{figure}

\newpage

The bearing for the simple homerange model is made relative to the homerange centre for individual \textit{i}. We use the Von Mises circular distribution to do this, which requires bearing to be in radians with the domain $[-\pi, \pi]$. That is, north is 0, and bearings to the west of north range from 0 to $-\pi$, and bearings to the east range from 0 to $\pi$. At each time step, a random bearing is drawn from a von Mises distribution as follows:

\begin{equation}
bearing_{i,t} \sim vonMises(\mu, \kappa)
\end{equation}

\begin{equation}
\kappa = \log(dist_i^\alpha)
\label{eq:hrKappa}
\end{equation}

\noindent
where $\mu$ is the direction in radians to the homerange centre, $dist_i$ is the distance from the current location of stoat \textit{i} to its homerange centre, and $\kappa$ is a parameter dictating the focus of pull toward the point of attraction (\textit{i.e.} homerange centre), which is a function is $dist_i$ and $\alpha$ a distance decay parameter. As distance from the homerange centre increases, the focus of pull increases, and $\alpha$ acts to accentuate or tenuate the effect of distance on the force of pull toward the homerange centre. In Python, this can be called as: 

\begin{equation}
bearing_{i,t} = np.random.vonmises(\mu, \kappa).
\end{equation}


\begin{flushleft}
\begin{large}
\textbf{Mate-searching movement model}
\end{large}
\end{flushleft}

Mate-searching movement behaviour is adopted by stoats to increase the probability of encountering a mate. All stoats exhibit simple homerange movement behaviour except with the following conditions. Pregnant females always have simple homerange behaviour, but estrous females follow the biased random walk (BRW) described below. When one or more estrous females are present or pheromone decoys have been deployed, both estrous females and reproductive males will adopt a BRW movement behaviour (see Appendix 1 for a decision tree for the two movement modes). 

When an stoat is in an 'estrous' state, the centre of attraction (COA) is no longer its homerange centre, and is instead a selected estrous female or pheromone decoy (Figure \ref{fig:turnangles}B). The selection of the (COA) among multiple choices will be discussed below. When far away from the selected COA, the step lengths are drawn from the Weibull as above with the homerange movement behaviour. The $\mu$ parameter of the von Mises distribution is adjusted to the bearing toward the COA. The $\kappa$ parameter of the von Mises is calculated as a function of distance from the COA, and days since deployment when the COA is a pheromone decoy (Figure \ref{fig:distTimeDecay}). It is calculated as follows:

\begin{equation}
\kappa = \left(\frac{1.0}{minK}\right)e^{-\delta dist}e^{-\gamma days}
\label{eq:matesearchTA}
\end{equation}


\noindent
where \textit{minK} is the set minimum $\kappa$ values (\textit{e.g.} 0.005), \textit{dist} is the distance in metres from the location of a stoat to its COA, $\delta$ is the parameter dictating how attraction to the COA decays with distance (\textit{e.g.} 0.001), \textit{days} is the number of days since pheromone decoy deployment, and $\gamma$ is the pararmeter determining how attraction decays with time (\textit{e.g.} 0.01).

To model the convergence of a stoat on a COA (\textit{e.g.} a mate or a decoy), we allow the scale parameter be reduced as a stoat approches its COA. We do this by making the scale parameter of the Weibull half the distance to the COA when the distance is less than the set scale value (\textit{e.g.} 70m). 


\begin{figure}[h!]
\includegraphics[scale=.50, left]{distTimeDecay.png}
\caption{The $\kappa$ parameter, (\textit{i.e.} the focus of directional movement), decreases with increasing days since deployment (varying thickness of lines), and distance from the centre of attraction.}
\label{fig:distTimeDecay}
\end{figure}








\begin{flushleft}
\begin{large}
\textbf{Selecting one among many potential centre of attractions}
\end{large}
\end{flushleft}

A stoat selects a COA by comparing the force of attraction from all potential centres of attraction within a 1-km radius. If no stoats of the opposite sex or pheromone decoys are within the 1-km detection radius, the $bearing_{i,t}$ is the result of a random draw from a uniform $[-\pi, \pi]$. The force of attraction is determined by the $\kappa$ parameter of the potential COA, which is a function of distance and time since deployment. Estrous females have a 0.0 value for time since deployment, as their attractiveness will not decline with time. For each stoat at each time step the COAs have to be assessed for their $\kappa$ values (see above). The selection $COA_i$ for stoat \textit{i} is done with a choose 1 multinomial draw where an array of the probabilities of selection \textbf{P(sel)} are derived from all potential $\kappa$ values:


\begin{equation}
COA_i \sim multinomial(1, \textbf{P(sel)}).
\end{equation}

\begin{equation}
P(sel)_c = \frac{\kappa_c}{\sum_{c=1}^{C}\kappa_c}
\end{equation}

\noindent
where $\kappa_c$ is the $\kappa$ value for COA \textit{c} among a total of \textit{C} potential COAs.

\begin{flushleft}
\begin{large}
\textbf{Mating with an estrous female or interacting with a pheromone decoy}
\end{large}
\end{flushleft}

A females will adopt a mate searching behaviour if she is not pregnant at the onset of the estrous period (15 September - 15 January; set in params). If she is pregnant on 15 September, she maintains homerange movement behaviour and is not attractive to males. The pheromone decoys will only be deployed during this period (see details below on deployment). 

At each time step during the estrous period, an \textit{encounter} occurs between stoat \textit{i} and COA \textit{i} if they are within a set \textit{encounterDistance} (\textit{e.g.} 10 m set in params). If there are multiple COAs that are encountered, $stoat_i$ will select to interact with a stoat of the opposite sex over a decoy. If an \textit{encounter} occurs with a pheromone decoy, $stoat_i$ will not be attracted to that decoy for a set number of days (\textit{habituationDays}; 15 days). Further, a male that interacts with a female will not be attracted to that female for same \textit{habituationDays}. 

If an encounter occurs with an estrous female, she is mated but only becomes pregnant with a random chance using a Bernouilli trial as follows:

\begin{equation}
preg_i \sim Bernouilli(0.667).
\end{equation}

\noindent
If the female becomes pregnant, she can remain in receptive to males, and multiple males can sire kits in her next litter. However, if she is pregnant, she resumes homerange behaviour and does not seek new mates. For the duration of the estrous period (15 Sept - 15 Jan) males will still be attracted to her and she can act as a COA for them. 


If a female becomes pregnant, delayed implantation has to occur and she will remain pregnant until the following Oct 30th, at which time she will give birth. Even if she becomes pregnant on 15 September she has to undergo delayed implantation and give birth the following year. The number of kits (\textit{nKits}) is determined from a random draw from a Poisson distribution with mean (\textit{meanRecruits; e.g.} 9 set in params):

\begin{equation}
nKits \sim Poisson(meanRecruits)
\end{equation}
\begin{equation}
nFemaleKits \sim binomial(nKits, \, 0.5)
\end{equation}
\begin{equation}
nMaleKits = nKits - nFemaleKits
\end{equation}

About 10 days after the birth the female will again come into estrous if it is still in the estrous window (15 September - 15 January). However, she will maintain homerange movement behaviour because she will have kits to care for. The adult female and the female kits are available for mating until 15 January, but all will exhibit homerange movement behaviour.  


\begin{flushleft}
\begin{large}
\textbf{Offspring sexual maturity and dispersal}
\end{large}
\end{flushleft}

Female offspring can be mated at a very early age while still in the nest. Males do not become sexually mature until about 12 months, or the following estrous period. Male and female offspring will disperse on 16 January, and will disperse anywhere on the island (we could change this later for larger and more complex landscapes). Between birth and dispersal, we will not model movement behaviour of the offspring. For simplicity, we will assume that they stay at the nest and the adult female provides for them. However, the female offspring in the nest can act as a single COA in the estrous period for males to come and mate. As stated previously, the adult female will exhibit homerange behaviour, but we will not explicitly model returning to the nest. If the adult female gets caught in a trap before the offspring disperse, she and all offspring die and are removed from the population.

If the female offspring are mated after birth and before 15 January they become pregnant with a Bernouilli trial as follows:

\begin{equation}
preg_i \sim Bernouilli(0.667).
\end{equation}

They will not give birth until the following 30 October (assuming survival). The number of male and female offspring will be determined as described above. 

\begin{flushleft}
\begin{large}
\textbf{Natural mortality and trapping}
\end{large}
\end{flushleft}

The existing Resolution Island trapping network is deployed across the island. There are three trapping session: 20 January; 20 July; and 20 November. The bait in the traps is attractive for only 14 days, then the traps are no longer effective. At the end of each hourly movement step, a stoat encounters a trap if a baited trap is within the set distance \textit{trapEncDist} (\textit{e.g.} 10 m set in params) of its location. It will interact with the trap and die with a random Bernouilli trial:

$$capture_i \sim Bernouilli(\tau)$$

\noindent
where $\tau$ is the probability of interaction given an encounter (\textit{e.g.} 0.25; set in params). If it is captured, it is removed from the population. 

At the end of each day, if a stoat is not captured by a trap, daily survival is determined with a random Bernouilli trial as follows:

\begin{equation}
survival_t \sim Bernouilli(PDaySurv)
\end{equation}

\begin{equation}
PDaySurv = PAnnualSurv^{(1/365)}
\end{equation}

\noindent
where $PAnnualSurv$ is the annual probability of survival (\textit{e.g.} 0.45; set in params).

\begin{flushleft}
\begin{large}
\textbf{End of iteration}
\end{large}
\end{flushleft}

If the population ever goes to 0 individuals, the process stops. At the end of the iteration, a data array is populated to be used in a statisical analysis. The response variable is whether or not eradication occurred. Parameters or variables to capture:

\begin{flushleft}
\begin{enumerate}
\item Eradication (1, 0)
\item meanNAdd
\item Decoy spacing
\item Number of decoy deployments
\item $\alpha$
\item $\gamma$
\item $\delta$
\item \textit{habituationDays}
\end{enumerate}
\end{flushleft}



\includepdf[pages={-}]{decisionTree_Movement.pdf}


\end{document}