#!/usr/bin/env python

import os
import tempfile
import subprocess
import pickle
import datetime
import shutil
import numpy as np
from numba import njit
from osgeo import gdal

FRAMERATE = 10

def makeMovie(params, stoatDebugFrame, stoatDebugInEstrous, 
                    stoatDebugDaysSincePheromone, trappingDays,
                    pheromoneArray, trapsArray, outputDataPath):

    nDays = (params.endDate - params.startDate).days
    ds = gdal.Open(params.extentMask)
    transform = ds.GetGeoTransform()
    mask = ds.GetRasterBand(1).ReadAsArray()
    del ds

    ds = gdal.Open('data/fonts.gif')
    fonts = ds.GetRasterBand(1).ReadAsArray()
    del ds

    tempDir = tempfile.mkdtemp()
    frameN = 0
    nstoats = 0
    for day in range(nDays):
        for hour in range(params.hoursPerDay):
            i = (day * params.hoursPerDay) + hour
            dateobj = params.startDate + datetime.timedelta(days=day)
            pheromoneStrength = 0.0

            inEstrous = stoatDebugInEstrous[i]
            daysSincePheromoneRelease = stoatDebugDaysSincePheromone[i]

            if inEstrous and daysSincePheromoneRelease != -1:
                pheromoneStrength = (np.exp(-params.COA_decay_temporal * daysSincePheromoneRelease)) / (np.exp(-params.COA_decay_temporal))

            traps = (day in trappingDays)
            #print(day, hour)
            if hour == 0:
                frameIMG, nstoats = createFrame(tempDir, frameN, fonts, stoatDebugFrame[i], 
                        transform, mask, trapsArray, pheromoneArray, dateobj, hour,
                        pheromoneStrength, traps)
                if nstoats == 0:
                    break
                frameN += 1

        if nstoats == 0:
            break

    # make movie
    movieName = os.path.join(outputDataPath, 'movie.wmv')
    subprocess.check_call(['ffmpeg', '-framerate', str(FRAMERATE), '-i', 
        os.path.join(tempDir, 'frame_%03d.tif'),
        '-vcodec', 'mpeg4', '-q:v', '1', '-y', 
        '-loglevel', 'error', movieName])

    #print(tempDir)
    shutil.rmtree(tempDir)

@njit
def createFrameSquares(data, cx, cy, colour, size=3):
    cx = int(cx)
    cy = int(cy)
    b, ysize, xsize = data.shape
    for x in range(cx - size, cx + size):
        for y in range(cy - size, cy + size):
            if x >= xsize:
                x = xsize - 1
            if x < 0:
                x = 0
            if y >= ysize:
                y = ysize - 1
            if y < 0:
                y = 0
            data[0, y, x] = colour[0]
            data[1, y, x] = colour[1]
            data[2, y, x] = colour[2]

@njit
def createFrameCircles(data, cx, cy, colour, size=6):
    cx = int(cx)
    cy = int(cy)
    b, ysize, xsize = data.shape
    for x in range(cx - size, cx + size):
        for y in range(cy - size, cy + size):
            if x >= xsize:
                x = xsize - 1
            if x < 0:
                x = 0
            if y >= ysize:
                y = ysize - 1
            if y < 0:
                y = 0

            xdist = x - cx
            ydist = y - cy
            dist = np.sqrt(xdist * xdist + ydist * ydist)
            if dist > size:
                continue

            data[0, y, x] = colour[0]
            data[1, y, x] = colour[1]
            data[2, y, x] = colour[2]
    
@njit
def copyChar(data, ordval, x, y, fonts):
    ysize, xsize = fonts.shape
    src_x = int(ordval % 16) * 14
    src_y = int(ordval / 16) * 14
    for i in range(28):
        for j in range(28):
            # 0 where there is data
            src_y_t = src_y + int(j / 2)
            src_x_t = src_x + int(i / 2)
            if src_y_t < ysize and src_x_t < xsize:
                if fonts[src_y_t, src_x_t] == 0:
                    data[0, y + j, x + i] = 255
                    data[1, y + j, x + i] = 255
                    data[2, y + j, x + i] = 0

def createFrame(tempdir, frameN, fonts, info, transform, mask, trapsArray, 
        pheromoneArray, dateobj, hour, pheromoneStrength, includeTraps):

    ysize, xsize = mask.shape
    frameIMG = os.path.join(tempdir, 'frame_%03d.tif' % frameN)
    driver = gdal.GetDriverByName('GTiff')
    ds = driver.Create(frameIMG, xsize, ysize, 3, gdal.GDT_Byte)
    ds.SetGeoTransform(transform)
    tinverse = gdal.InvGeoTransform(transform)

    data = np.zeros((3, ysize, xsize), dtype=np.uint8)
    # burn in mask
    for b in range(3):
        data[b] = np.where(mask != 0, 255, 0)

    # stoats
    colour = np.array([255, 0, 0])  # red
    nstoats = 0
    for i in range(info.shape[0]):
        if not info[i]['deleted']:
            nstoats += 1
            cx, cy = gdal.ApplyGeoTransform(tinverse, info[i]['x'], info[i]['y'])
            if info[i]['male']:
                colour[0] = 0 # black
                colour[1] = 0
                colour[2] = 0
            elif not info[i]['pregnant']:
                colour[0] = 0   
                colour[1] = 0   
                colour[2] = 255 # blue
            else:
                colour[0] = 255 # red
                colour[1] = 0
                colour[2] = 0
                
            # size varies on homerange state
            if info[i]['homerange']:
                size = 6 # small
            else:
                size = 9 # big
                
            createFrameCircles(data, cx, cy, colour, size)

    if includeTraps:
        colour = np.array([0, 255, 0])
        for i in range(trapsArray.shape[0]):
            cx, cy = gdal.ApplyGeoTransform(tinverse, trapsArray[i, 0], trapsArray[i, 1])
            createFrameSquares(data, cx, cy, colour)

    if pheromoneStrength > 0:
        oneMinus = 1.0 - pheromoneStrength
        colour = np.array([int(255 * oneMinus), int(255 * oneMinus), 255])
        for i in range(pheromoneArray.shape[0]):
            cx, cy = gdal.ApplyGeoTransform(tinverse, pheromoneArray[i]['x'], pheromoneArray[i]['y'])
            createFrameSquares(data, cx, cy, colour)

    # text
    str = '%s hour: %d' % (dateobj.isoformat(), hour)
    x = 30
    y = 30
    for val in str:
        copyChar(data, ord(val), x, y, fonts)
        x += 28

    for b in range(3):
        band = ds.GetRasterBand(b+1)
        band.WriteArray(data[b])

    del ds

    return frameIMG, nstoats

if __name__ == '__main__':

    outputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'pheromoneWork', 
        'Results', 'mod1_testmulti')

    pkl = open(os.path.join(outputDataPath, 'stoatsparams.pkl'), 'rb')
    params = pickle.load(pkl)
    pkl.close()
    with np.load(os.path.join(outputDataPath, 'stoats.npz')) as data:
        makeMovie(params, data['debugInfo'], data['inEstrous'], 
                data['daysSincePheromone'], data['trappingDays'],
                data['pheromones'], data['traps'], outputDataPath)

