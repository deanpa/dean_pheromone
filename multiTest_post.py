#!/usr/bin/env python

import os
import pickle
from pheromone import calcresults
#from pheromone import params
from scipy.stats.mstats import mquantiles
import pylab
import numpy as np


def processResults():
    ### Output data path to write graphs, imagees and movies
    outputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 
            'pheromoneWork', 'Results', 'mod1_testmulti')

    resultsDataPath = os.path.join(outputDataPath, 'results.pkl')

    simResultsFile = os.path.join(outputDataPath, 'simulationResults.csv')    



    results = calcresults.PheromoneResults.unpickleFromFile(resultsDataPath)

    calcresults.PheromoneResults.writeToFileFX(results, simResultsFile)


#    for i in range(len(results)):
#        print('i', results[i].eradicated, results[i].nAdd, results[i].decoySpacing, 
#            results[i].nDecoyDeplyment,
#            results[i].alphaK, results[i].COA_decay_spatial, results[i].COA_decay_temporal,
#            results[i].habituationDays, results[i].pDaySurv)


if __name__ == '__main__':
    processResults()
