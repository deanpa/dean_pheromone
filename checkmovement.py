#!/usr/bin/env python

import pickle
import datetime
import numpy as np

STARTDATE = datetime.date(2019, 2, 1)
ENDDATE = datetime.date(2019, 2, 15)
MAXSTOATS = 5

def getids(params, stoatDebugFrame):
    """
    Return the ids of stoats alive on STARTDATE and ENDDATE
    """

    startids = None
    endids = None
    nDays = (params.endDate - params.startDate).days
    for day in range(nDays):
        for hour in range(params.hoursPerDay):
            i = (day * params.hoursPerDay) + hour
            dateobj = params.startDate + datetime.timedelta(days=day)
            if dateobj == STARTDATE:
                # ok find all the non-deleted ids
                mask = np.logical_not(stoatDebugFrame[i]['deleted'])
                startids = stoatDebugFrame[i][mask]['id']
            elif dateobj == ENDDATE:
                # ok find all the non-deleted ids
                mask = np.logical_not(stoatDebugFrame[i]['deleted'])
                endids = stoatDebugFrame[i][mask]['id']

                # stoats that are alive in both
                commonids = set(startids) & set(endids)
                commonids = np.array(list(commonids))
                if len(commonids) > MAXSTOATS:
                    commonids = commonids[:MAXSTOATS]
                commonids.sort()
                return commonids

    
def doDump(ids, params, stoatDebugFrame):
    """
    Dump the coords of stoats in the ids array between STARTDATE
    and ENDDATE.
    """
    nDays = (params.endDate - params.startDate).days
    for day in range(nDays):
        for hour in range(params.hoursPerDay):
            i = (day * params.hoursPerDay) + hour
            dateobj = params.startDate + datetime.timedelta(days=day)
            if dateobj >= STARTDATE and dateobj <= ENDDATE:
                for stoatid in ids:
                    mask = stoatDebugFrame[i]['id'] == stoatid
                    stoat = stoatDebugFrame[i][mask][0]
                    s = '%d,%d,%d,%f,%f' % (day, hour, stoat['id'], stoat['x'], stoat['y'])
                    print(s)
    

if __name__ == '__main__':
    pkl = open('stoatsparams.pkl', 'rb')
    params = pickle.load(pkl)
    pkl.close()

    with np.load('stoats.npz') as data:
        ids = getids(params, data['debugInfo'])
        doDump(ids, params, data['debugInfo'])

