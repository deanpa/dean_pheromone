#!/usr/bin/env python


#import paramsPredators
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
from scipy import stats
import prettytable
import pickle
import os
import datetime
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import calendar
import pandas as pd

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))

def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


class ResultsProcessing(object):
    def __init__(self, gibbsobj, basicobj, params, path):

        self.params = params
        self.path = path
        self.gibbsobj = gibbsobj
        self.basicobj = basicobj
        self.ndat = len(self.gibbsobj.betagibbs)
        self.results = self.gibbsobj.betagibbs
        self.npara = self.results.shape[1]
        self.data =  pd.read_csv(self.params.inputObsFname, header = 0)
        print('total iter', ((self.ndat * self.params.thinrate) + 
                self.params.burnin))
        print('thin = ', self.params.thinrate, 'burn = ', self.params.burnin)


    def makeTableFX(self):
        self.ncol = np.shape(self.results)[1]
        
        #Model 1 = full model (9 parameters)
        if self.params.modelID == 1:
            self.names = np.array(['intercept', 'nAdd', 'decoySpacing', 'nDeploy', 'alphaK', 
                                   'coaSpatialDecay', 'coaTempDecay', 'habituationDays', 'pSurvive'])
            
        #Model 2: deployment covariates (5 parameters)
        if self.params.modelID == 2:
            self.names = np.array(['intercept', 'nAdd', 'decoySpacing', 'nDeploy', 
                                   'habituationDays'])
                    
        #Model 3: movement covariates (4 parameters)
        if self.params.modelID == 3:
            self.names = np.array(['intercept', 'alphaK', 'coaSpatialDecay', 'coaTempDecay'])
            
        #Model 4: deployment spacing and frequency (3 parameters)
        if self.params.modelID == 4:
            self.names = np.array(['intercept', 'decoySpacing', 'nDeploy'])
                       
        # Model 5: Biological covarites (3 parameters)
        if self.params.modelID == 5:
            self.names = np.array(['intercept', 'nAdd', 'pSurvive'])
            
        resultTable = np.zeros(shape = (3, self.ncol))
        resultTable[0] = np.round(np.mean(self.results[:, :self.ncol], axis = 0), 3)
        resultTable[1:3] = np.round(mquantiles(self.results[:, :self.ncol], 
                prob=[0.05, 0.95], axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Mean', 'Low CI', 'High CI'])
        for i in range(self.ncol):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()

    ########            Write data to file
    ########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Mean', np.float),
                    ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        structured['Mean'] = self.summaryTable[:, 0]
        structured['Low CI'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Names'] = self.names
        np.savetxt(self.params.paramsResFname, structured, 
            fmt=['%s', '%.4f', '%.4f', '%.4f'],
                    comments = '', delimiter = ',', 
                    header='Names, Mean, Low_CI, High_CI')

    def tracePlotFX(self):
        """
        plot diagnostic trace plots
        """
        cc = 0
        plotNames = self.names
        plotArray = self.results
        nTotalFigs = self.ncol
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.tight_layout() 
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    P.plot(plotArray[:, cc])
                    P.title(plotNames[cc])
                cc += 1
            
            P.savefig(self.params.paramsTraceFigFname + str(i) + '.png', format='png', dpi=400)
            P.show(block=lastFigure)
            
    def predgraphsFX(self):
        Names = self.names
        ignore = self.names == 'ndeploy'
        coefficients = self.summaryTable[:, 0]
        if True in ignore: 
            nTotalFigs = self.ncol - 2 #no plotting the intercept or ndeploy
        else: 
            nTotalFigs = self.ncol - 1 #no plotting the intercept, no ndeploy in this model  
            
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        xvalues = []
        yvalues = []
        plotNames = []
        for var in range(1, len(coefficients)): 
            if ignore[var] != True:
                print(Names[var])
                plotNames.append(Names[var])
                xvalues_var = np.linspace(np.min(self.basicobj.CovarMatrix[:,var]), np.max(self.basicobj.CovarMatrix[:,var]))
                pred = coefficients[0] + coefficients[var] * xvalues_var
                pred = inv_logit(pred)
                txvalues_var = xvalues_var * self.basicobj.covsds_red[var-1] + self.basicobj.covmeans_red[var-1]
                if Names[var] == 'pSurvive':
                     txvalues_var = np.power(txvalues_var, 365.0)
                xvalues.append(txvalues_var)
                yvalues.append(pred)
                
        cc = 0        
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2, 3, j+1)
                P.tight_layout() 
                if cc < nTotalFigs:
                    P.ylim(0, 1)
                    P.plot(xvalues[cc], yvalues[cc])
                    P.title(plotNames[cc])
                    cc += 1
                    print(cc)
            P.savefig(self.params.paramsPredFigFname + str(i) + '.png', format='png', dpi=400)
            P.show(block=lastFigure)

    def calcDIC(self):
        self.dBar = np.mean(self.gibbsobj.Dgibbs)
        beta_s = np.mean(self.gibbsobj.betagibbs, axis = 0)
        obsDot_s = np.dot(self.basicobj.x, beta_s[1:])
        ## REL PROBABILITY OF eradication FOR ALL DATA
        w_s = beta_s[0] + obsDot_s
        theta = inv_logit(w_s)
        
        
        #Likelihood of data (erad) given parameter theta (which is calculated from the betas)
        logLikZ_s = np.sum(stats.binom.logpmf(self.basicobj.erad, 1, theta))
        ## CALCULATE DIC
        self.D_MeanTheta = -2.0 * logLikZ_s
        self.DIC = (2.0 * self.dBar) - self.D_MeanTheta  
        self.PD = self.dBar - self.D_MeanTheta

        ## CALCULATE DIC FOR NULL MODEL
        self.basicobj.NDat
                
        dBarNull = -2.0 * np.log(1.0 / 1001.0) * self.basicobj.NDat
        meanThetaNull = -2.0 * np.log(1.0 / 1001.0) * self.basicobj.NDat
        nullDIC = (2.0 * dBarNull) - meanThetaNull        
        sampleSizeDIC = dBarNull

        print('########################')
        print('##   model', self.params.modelID)
        print('##   number of parameters', len(self.names))
        print('##   DIC: ', self.DIC)
        print('##   PD: ', self.PD)
        print('##   nullDIC: ', nullDIC)
        print('##   sampleSizeDIC: ', sampleSizeDIC)
        print('##')
        print('########################')

          
            
            