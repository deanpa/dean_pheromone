#!/usr/bin/env python

########################################
########################################
# This file is part of Possum g0 and sigma analysis
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
import numpy as np
from scipy import stats
import pandas as pd
from pandas.plotting import scatter_matrix
import datetime
from numba import jit
import pylab as P

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


### Define global functions: ###
def logit(x):
    return np.log(x) - np.log(1 - x)


def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


class BasicData(object):
    def __init__(self, params):
        """
        Object to read in  data
        Import updatable params from params
        set initial values
        """
#        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
        #############################################
        ############ Run basicdata functions
        self.getParams(params)
        self.readData()
        self.makeCovariateData()
        self.makescatterplots()
        
        ############## End run of basicdata functions
        #############################################

    def getParams(self, params):
        """
        # move params parameters into basicdata
        """
        self.params = params
        self.beta = self.params.beta
         
    def readData(self):
        """
        ## READ IN DATA FROM CSV
        """
        self.obsDat = pd.read_csv(self.params.inputObsFname, header = 0)
        self.obsDat.head()
        print('Obs shp', self.obsDat.shape)
        
        self.eradicated = np.array(self.obsDat[['eradicated']]).flatten()
        self.erad = self.eradicated.astype(int)
        self.nAdd = np.array(self.obsDat[['nAdd']]).flatten()
        self.decoySpacing = np.array(self.obsDat[['decoySpacing']]).flatten()
        self.nDeploy = np.array(self.obsDat[['nDeploy']]).flatten()
        self.alphaK = np.array(self.obsDat[['alphaK']]).flatten()
        self.coaSpatialDecay = np.array(self.obsDat[['coaSpatialDecay']]).flatten()
        self.coaTempDecay = np.array(self.obsDat[['coaTempDecay']]).flatten()
        self.habituationDays = np.array(self.obsDat[['habituationDays']]).flatten()
        self.pSurvive = np.array(self.obsDat[['pSurvive']]).flatten()
        self.NDat = len(self.nAdd)       
         
    def makeCovariateData(self):
        """
        ## CREATE DATA STRUCTURES FOR COVARIATES
        """
        meannAdd = np.mean(self.nAdd)
        sdnAdd = np.std(self.nAdd)
        meandecoySpacing = np.mean(self.decoySpacing)
        sddecoySpacing = np.std(self.decoySpacing)
        meanalphaK = np.mean(self.alphaK)
        sdalphaK = np.std(self.alphaK)
        meancoaSpatialDecay = np.mean(self.coaSpatialDecay)
        sdcoaSpatialDecay = np.std(self.coaSpatialDecay)
        meancoaTempDecay = np.mean(self.coaTempDecay)
        sdcoaTempDecay = np.std(self.coaTempDecay)
        meanhabituationDays = np.mean(self.habituationDays)
        sdhabituationDays = np.std(self.habituationDays)
        meanpSurvive = np.mean(self.pSurvive)
        sdpSurvive = np.std(self.pSurvive)
        
        self.covmeans = [0, meannAdd, meandecoySpacing, 0, meanalphaK, 
                                  meancoaSpatialDecay, meancoaTempDecay, meanhabituationDays, meanpSurvive]
        self.covmeans = np.array(self.covmeans).flatten()
        self.covsds = [0, sdnAdd, sddecoySpacing, 0, sdalphaK, sdcoaSpatialDecay, sdcoaTempDecay, sdhabituationDays, sdpSurvive]
        self.covsds = np.array(self.covsds).flatten()
        
        ## SCALE PARAMETERS
        ## DATA
        self.xDatObsFull = np.zeros((self.NDat, len(self.params.xdatDictionary)+1)) 
        self.xDatObsFull[:,0] = 1
        self.xDatObsFull[:,1] = (self.nAdd - meannAdd) / sdnAdd 
        self.xDatObsFull[:,2] = (self.decoySpacing - meandecoySpacing) / sddecoySpacing 
        self.xDatObsFull[:,3] = 0
        self.xDatObsFull[:,4] = (self.alphaK - meanalphaK) / sdalphaK
        self.xDatObsFull[:,5] = (self.coaSpatialDecay - meancoaSpatialDecay) / sdcoaSpatialDecay 
        self.xDatObsFull[:,6] = (self.coaTempDecay - meancoaTempDecay) / sdcoaTempDecay 
        self.xDatObsFull[:,7] = (self.habituationDays - meanhabituationDays) / sdhabituationDays 
        self.xDatObsFull[:,8] = (self.pSurvive - meanpSurvive) / sdpSurvive 
        
        for i in range(self.NDat):
            ndeploy = self.nDeploy[i]
            if ndeploy == 2:
                self.xDatObsFull[i,3] = 1
            
        
        ## MAKE REDUCED COVARIATE ARRAY FOR MULTIMODEL ANALYSIS
        self.x = self.xDatObsFull[:, self.params.xdatIndx]   
        self.CovarMatrix = np.hstack((np.ones((self.NDat, 1)), self.x))
        print('number of parameters to estimate in this model: ' + str(self.CovarMatrix.shape[1]))
        
        self.covmeans_red = self.covmeans[self.params.xdatIndx]
        self.covsds_red = self.covsds[self.params.xdatIndx]
        
        ## COVARIATE MATRIX MULTIPLICAION
        self.obsDot = np.dot(self.CovarMatrix, self.beta)
        self.theta = inv_logit(self.obsDot)
        #Likelihood of data (erad) given parameter theta (which is calculated from the betas)
        self.logLiktheta = np.sum(stats.binom.logpmf(self.erad, 1, self.theta))

    def makescatterplots(self): 
        self.rawdata = self.obsDat[["nAdd", "decoySpacing", 'nDeploy', 'alphaK', 
                                    'coaSpatialDecay', 'coaTempDecay', 'habituationDays',
                                    'pSurvive']]
        P.tight_layout() 
        fig = self.rawdata.plot(subplots = True, layout=(3, 3), figsize=(14, 14), sharex=False, style='.', 
                          legend="none")
        
            
        figname = os.path.join(self.params.inputDataPath , 'IndexPlots.png')
        P.savefig(figname, format='png', dpi=400)
        
        P.tight_layout() 
        scatter_matrix(self.rawdata, alpha=0.2, figsize=(6, 6), diagonal="kde")
        figname = os.path.join(self.params.inputDataPath , 'CorrelationPlots.png')
        P.savefig(figname, format='png', dpi=400)