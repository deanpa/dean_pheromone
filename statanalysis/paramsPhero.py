#!/usr/bin/env python

########################################
########################################
# This file is part of TBfree funded project to quantify g0 and
# sigma parameters for possums across diverse habitats
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
#import initiateModel
#import mcmcRun
import numpy as np


class ModelParams(object):
    """
    Contains the parameters for the mcmc run. This object is also required 
    for the pre-processing. 
    """
    def __init__(self):
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        #################################
        self.modelID = 1 
        print('Running model:', self.modelID)
        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 5000          #3000  # number of estimates to save for each parameter
        self.thinrate = 20          #20   # 200      # thin rate
        self.burnin = 5000          # 8000 burn in number of iterations

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = True   ## True or False         
        print('First Run:', self.firstRun)

        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)

        # set paths to scripts and data
        self.inputDataPath = 'E:\Contracts\LCR\StoatPheromone\Results'
        self.outputDataPath = 'E:\Contracts\LCR\StoatPheromone\Results\Mod' + str(self.modelID)


        ## set Data names
        self.basicdataFname = os.path.join(self.outputDataPath, 'basicdataMod' + str(self.modelID) + '.pkl')
        self.mcmcFname = os.path.join(self.outputDataPath, 'mcmcMod' + str(self.modelID) + '.pkl')
        self.paramsResFname = os.path.join(self.outputDataPath, 'parameterTableMod' + str(self.modelID) + '.csv')
        self.paramsTraceFigFname = os.path.join(self.outputDataPath, 'Mod' + str(self.modelID) + 'TracePlot_')
        self.paramsPredFigFname = os.path.join(self.outputDataPath, 'Mod' + str(self.modelID) + 'PredPlot_')
         
        ## Input data
        self.inputObsFname = os.path.join(self.inputDataPath, 'simulationResults.csv')

        print('params basic path', self.basicdataFname) 
        print('ngibbs', self.ngibbs, 'thin', self.thinrate, 'burnin', self.burnin)

        ## Could add in a data dictionary for defining covariates - later
        
        ######################################
        ######################################
        # modify  variables used for statistical model
        self.xdatDictionary = {'nAdd' : 1, 'decoySpacing' : 2, 
                               'nDeploy' : 3, 'alphaK' : 4,
                               'coaSpatialDecay' : 5, 'coaTempDecay' : 6, 
                               'habituationDays' : 7, 'pSurvive' : 8}
        self.scalenAdd = self.xdatDictionary['nAdd']
        self.scaledecoySpacing = self.xdatDictionary['decoySpacing']
        self.scalenDeploy = self.xdatDictionary['nDeploy']
        self.scalealphaK = self.xdatDictionary['alphaK']
        self.scalecoaSpatialDecay = self.xdatDictionary['coaSpatialDecay']
        self.scalecoaTempDecay = self.xdatDictionary['coaTempDecay']
        self.scalehabituationDays = self.xdatDictionary['habituationDays']
        self.scalepSurvive = self.xdatDictionary['pSurvive']
        
        # array of index values to get variables from 2-d array
        #Model 1 = full model (9 parameters)
        if self.modelID == 1:
            self.xdatIndx = np.array([self.scalenAdd, self.scaledecoySpacing, self.scalenDeploy, self.scalealphaK, 
                                      self.scalecoaSpatialDecay, self.scalecoaTempDecay,
                                      self.scalehabituationDays, self.scalepSurvive], dtype = int)
            
        #Model 2: deployment covariates (5 parameters)
        if self.modelID == 2:
            self.xdatIndx = np.array([self.scalenAdd, self.scaledecoySpacing, self.scalenDeploy,
                                      self.scalehabituationDays], dtype = int)
        
        #Model 3: movement covariates (4 parameters)
        if self.modelID == 3:
            self.xdatIndx = np.array([self.scalealphaK, self.scalecoaSpatialDecay, self.scalecoaTempDecay], dtype = int)
            
        #Model 4: deployment spacing and frequency (3 parameters)
        if self.modelID == 4:
            self.xdatIndx = np.array([self.scaledecoySpacing, self.scalenDeploy], dtype = int)
            
        # Model 5: Biological covarites (3 parameters)
        if self.modelID == 5:
            self.xdatIndx = np.array([self.scalenAdd, self.scalepSurvive], dtype = int)
            
        ######################################
        ######################################

        ## SET INITIAL PARAMETER VALUES
        ### beta parameters: INTERCEPT, <<<nAdd, decoyS, nDeploy, alphaK, 
        ### coaSpatial, coaTemp, habdays, psurv>>>  
        self.beta = np.repeat(0.01, len(self.xdatIndx))
        self.beta = np.hstack([2, self.beta])
        self.nBeta = len(self.beta)
        self.priorBeta = [0, 10.0]
        self.searchBeta = 0.03      






