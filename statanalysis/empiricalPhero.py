#!/usr/bin/env python

########################################
########################################
# This file is part of OSPRI possum g0 and sigma project
# Copyright (C) 2018 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

### Import modules: ###
import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
from numba import jit
import pickle


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


def meanGibbsUpdate(y, n, var, prior):
    """
    Gibbs updater for means
    """
    priorVarInv = 1.0 / prior[1]
    nVarInv = n / var
    V = (priorVarInv + nVarInv)**-1
    priorMeanVar = prior[0] / prior[1]
    nyVarInv = n * np.mean(y) / var
    v = priorMeanVar + nyVarInv
    muOut = np.random.normal(v*V, V)
    return(muOut) 
    

def varGibbsUpdate(s1, s2, y, mu, n):
    predDiff = y - mu
    sx = np.sum(predDiff**2.0)
    u1 = (n / 2.0) + s1
    u2 = s2 + (0.5 * sx)
    iVar = np.random.gamma(u1, u2)
    return(1.0 / iVar)



class MCMC(object):
    def __init__(self, params, basicdata):

        self.basicdata = basicdata
        self.params = params
        self.betagibbs = np.zeros((self.params.ngibbs, self.params.nBeta))             # beta parameters
        self.Dgibbs = np.zeros(self.params.ngibbs) 
        
        ## run mcmcFX - gibbs loop
        self.mcmcFX()

    def beta_UpdateFX(self):
        beta_s = np.random.normal(self.basicdata.beta, self.params.searchBeta)
        obsDot_s = np.dot(self.basicdata.CovarMatrix, beta_s)
        theta_s =  inv_logit(obsDot_s)
        ## likelihood of theta_s (derived from the proposed beta_s) given the data
        logLiktheta_s = np.sum(stats.binom.logpmf(self.basicdata.erad, 1, theta_s))
        ## BETA PRIORS
        prior = np.sum(stats.norm.logpdf(self.basicdata.beta, self.params.priorBeta[0],
            self.params.priorBeta[1]))
        prior_s = np.sum(stats.norm.logpdf(beta_s, self.params.priorBeta[0],
            self.params.priorBeta[1]))
        ##CALCULATE IMPORTANCE RATIO
        pnow = self.basicdata.logLiktheta + prior
        pnew = logLiktheta_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        #print('present', np.sum(self.basicdata.logLiktheta), prior, np.round(pnow,2), rValue > zValue)
        #print('new Val', np.sum(logLiktheta_s), prior_s, np.round(pnew, 2), rValue > zValue)
        ## UPDATE MODEL ELEMENTS
        if (rValue > zValue):
            self.basicdata.beta = beta_s.copy()
            self.basicdata.obsDot = obsDot_s.copy()
            self.basicdata.theta = theta_s.copy()
            self.basicdata.logLiktheta = logLiktheta_s.copy()
            
    ########            Main mcmc function
    ########
    def mcmcFX(self):

        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):
            print('g', g)
            print(self.basicdata.beta.shape)
            # betas for predicting sigma
            self.beta_UpdateFX()
            
            ## POPULATE ARRAYS FOR POST PROCESSING
            if g in self.params.keepseq:
                self.betagibbs[cc] = self.basicdata.beta
                self.Dgibbs[cc] = -2.0 * self.basicdata.logLiktheta
                cc = cc + 1





