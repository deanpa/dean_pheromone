#!/usr/bin/env python


from statanalysis.paramsPhero import ModelParams
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
#import datetime
#from numba import jit
from statanalysis import empiricalResultsPhero

def main():
    params = ModelParams()
    print('########################')
    print('########################')
    print('###')
    print('#    Model ' + str(params.modelID))
    print('###')
    print('########################')
    print('########################')

    # paths and data to read in

    path = 'E:\Contracts\LCR\StoatPheromone\Results\ModALL2' + str(params.modelID)

    fileobj = open(params.mcmcFname, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    fileobj = open(params.basicdataFname, 'rb')
    basicobj = pickle.load(fileobj)
    fileobj.close()

    print('gibbsResults Name:', params.mcmcFname)    

    resultsobj = empiricalResultsPhero.ResultsProcessing(gibbsobj, basicobj, params, path)

    resultsobj.makeTableFX()

    resultsobj.writeToFileFX()

    resultsobj.tracePlotFX()

    resultsobj.predgraphsFX()

    resultsobj.calcDIC()

if __name__ == '__main__':
    main()


