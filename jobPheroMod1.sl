#!/bin/bash

#SBATCH --job-name=multiTest1
#SBATCH --account=landcare00045
#SBATCH --mail-type=end
#SBATCH --mail-user=deanpa@gmail.com
#SBATCH --time=00:25:00

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=3000  

export RIOS_DFLT_JOBMGRTYPE=slurm
export RIOS_SLURMJOBMGR_SBATCHOPTIONS="--job-name=subMod1 --account=landcare00045 --time=00:12:00 --mem-per-cpu=3000"
export RIOS_SLURMJOBMGR_INITCMDS="export PYTHONPATH=$PWD;module load Python-Geo/3.7.3-gimkl-2018b"

module load Python-Geo/3.7.3-gimkl-2018b

./testmulti.py

