#!/usr/bin/env python

import pickle
import datetime
import numpy as np

def printTrapping(params, trappingData):
    nDays = (params.endDate - params.startDate).days
    for day in range(nDays):
        dateobj = params.startDate + datetime.timedelta(days=day)
        ntrapped = trappingData[day]
        if ntrapped > 0:
            s = '%d,%d,%d,%d' % (dateobj.year, dateobj.month, dateobj.day, ntrapped)
            print(s)

if __name__ == '__main__':
    pkl = open('stoatsparams.pkl', 'rb')
    params = pickle.load(pkl)
    pkl.close()

    with np.load('stoats.npz') as data:
        printTrapping(params, data['trappingCount'])
